#!/bin/sh
set -e

PYTHONPATH=/opt/gbh/backend:/opt/gbh/backend/gbh_core_src python -Wignore:Unverified /usr/local/bin/initcosmos.py

exec "$@"
