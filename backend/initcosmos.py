#!/usr/bin/env python

import socket
import os

from azure.cosmos import CosmosClient, DatabaseProxy, ContainerProxy, PartitionKey

def main():
    client = CosmosClient(
        url=os.environ["COSMOS_CONFIG_HOST"],
        credential=os.environ["COSMOS_CONFIG_MASTER_KEY"],
        connection_verify=False,
    )
    db = client.create_database_if_not_exists(
        os.environ["COSMOS_CONFIG_DATABASE_NAME"])
    config = db.create_container_if_not_exists(
        os.environ["COSMOS_CONFIG_CONTAINER_NAME"], PartitionKey("/partition_key"))
    config.upsert_item({
        "id": "gbh-local",
        "keys": [{
            "name": "DEBUG",
            "value": True
        }, {
            "name": "APP_SECRET",
            "value": "177B7291DECC9F939F96CB339E357"
        }, {
            "name": "JWT_SECRET_KEY",
            "value": "9C3DE7C58FFAC759E3E5E93FFF411"
        }, {
            "name": "JWT_TOKEN_LOCATION",
            "value": "headers"
        }, {
            "name": "JWT_ACCESS_TOKEN_EXPIRES",
            "value": 20
        }, {
            "name": "JWT_REFRESH_TOKEN_EXPIRES",
            "value": 259200
        }, {
            "name": "ES_ENDPOINT",
            "value": "https://elasticsearch:9200"
        }, {
            "name": "ES_USERNAME",
            "value": "elastic"
        }, {
            "name": "ES_PASSWORD",
            "value": "gbh"
        }, {
            "name": "NEO4J_URL",
            "value": "bolt://neo4j:gbh_neo4j@neo4j"
        }, {
            "name": "COSMOS_ENDPOINT",
            "value": os.environ["COSMOS_CONFIG_HOST"],
        }, {
            "name": "COSMOS_KEY",
            "value": os.environ["COSMOS_CONFIG_MASTER_KEY"],
        }, {
            "name": "COSMOS_CN_VERIFY",
            "value": False,
        },         {
            "name": "COSMOS_DATABASE",
            "value": "GBH"
        }, {
            "name": "COSMOS_COLLECTION",
            "value": "main"
        }],
        "partition_key": "config",
    })
    db.create_container_if_not_exists(
        "main", PartitionKey("/partition_key"))

    if os.environ.get("SKIP_INITIAL_DATA"):
        return

    from gbh_api.api.app import app
    from gbh_api.api.routes.users import user_service
    from gbh_api.api.routes.organizations import organization_service

    organization_service.upsert({
        "id": "4d57f6c8-8a77-4dce-9a27-87699a581d47",
        "name": "test_org",
    })

    user_service.upsert({
        "id": "2d58832f-4ad0-459c-b4a0-8351a22af7de",
        "name": "Tester",
        "email": "test@minjenv.nl",
        "password": "test",
        "role": "admin",
        "organization_summary": {
            "organization_id": "4d57f6c8-8a77-4dce-9a27-87699a581d47",
        },
    })


if __name__ == '__main__':
    main()
