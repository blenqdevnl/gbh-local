#!/bin/bash

set -e

npx ng build gbh-components

exec "$@"
